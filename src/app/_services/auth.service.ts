import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthData } from '../_models/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl = "https://hub.cobiro.com/v1/login";

  constructor(private http: HttpClient) { }

  login(payload: AuthData): Observable<any> {
    return this.http.post(this.baseUrl, payload)
  }
}
