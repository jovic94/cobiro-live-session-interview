import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthData } from 'src/app/_models/auth';
import { AuthService } from 'src/app/_services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  authData: AuthData = {};
  loginForm = new FormGroup({email: new FormControl(), password: new FormControl()})
  constructor(private authService: AuthService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: ["", [Validators.required]],
      password: ["", [Validators.required]]
    })
  }

  /**
   * Login:: Authenticates the user 
   */
  login() {
    this.authData.type = 'login';
    this.authData.attributes = {
      email: this.loginForm.controls.email.value,
      password: this.loginForm.controls.password.value
    } 
    this.authService.login(this.authData).subscribe(res => {
      console.log(res);
      if (res.success) {
        // Success Logic goes here
      } else {
        // Error logic goes here
      }
    })
  }

}
